import React, {Component} from 'react'
import {StyleSheet, View, TouchableOpacity, ScrollView} from 'react-native';
import {Button, Text} from 'native-base';
import IconMaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

import Theme from './../environments/Theme';
import Values from './../environments/Values';

import FirebaseServices from './../services/FirebaseServices';

export default class Profile extends Component {
  constructor(props) {
    super(props)
    this.state = {
      data: {}
    }
    FirebaseServices
      .getCurrentUser()
      .then((data) => {
        this.setState({data: data})
      })
  }

  _logout = () => {
    FirebaseServices.logout(this.props.navigation)
  }

  render() {
    let {data} = this.state

    return (
      <View style={Theme.styles.container}>
        <View style={[Theme.styles.w100]}>
          <Item label="Email address" value={data.email}/>
          <Item label="First name" value={data.firstname}/>
          <Item label="Last name" value={data.lastname}/>
          <Item label="Student ID" value={data.studentId}/>
          <Item label="Faculty" value={data.faculty}/>
          <Item label="Branch" value={data.branch}/>
          <Item label="Mobile No" value={data.mobile}/>
          <Button onPress={this._logout} full danger>
            <IconMaterialCommunityIcons
              size={Values.fontSize.icon}
              color={Values.colors.white}
              name='logout'/>
            <Text>Logout</Text>
          </Button>
        </View>
      </View>
    );
  }
}
class Item extends Component {
  render() {
    return (
      <View style={Theme.styles.profileRow}>
        <Text style={Theme.styles.profileLabel}>{this.props.label}</Text>
        <Text style={Theme.styles.profileValue}>{this.props.value}</Text>
      </View>
    );
  }
}