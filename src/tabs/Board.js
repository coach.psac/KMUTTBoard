import React, { Component } from 'react'
import { FlatList, StyleSheet, Text, TouchableOpacity, View } from 'react-native';


import Theme from './../environments/Theme';

import FirebaseServices from './../services/FirebaseServices';

export default class Board extends Component {
  constructor(props) {
    super(props);
    this.state = {
      boardData: []
    }
  }
  componentWillMount() {
    FirebaseServices.listenOrderByKey('board', (snap) => {
      var items = [];
      snap.forEach((data) => {
        items.push({
          boardKey: data.key,
          authorId: data
            .val()
            .authorId,
          studentId: data
            .val()
            .studentId,
          author: data
            .val()
            .author,
          subject: data
            .val()
            .subject,
          message: data
            .val()
            .message,
          date: data
            .val()
            .date
        });
      });
      this.setState({
        boardData: items.reverse()
      })
    });
  }
  render() {
    let { navigate } = this.props;
    return (
      <View style={Theme.styles.container}>
        <FlatList
          style={Theme.styles.w100}
          keyExtractor={(item, index) => item.boardKey}
          data={this.state.boardData}
          renderItem={({ item }) => <BoardItem item={item} navigate={navigate} />} />
      </View>
    );
  }
}

class BoardItem extends Component {
  render() {
    let { aurhorId, studentId, author, subject, date } = this.props.item;
    return (
      <TouchableOpacity
        activeOpacity={0.5}
        onPress={() => this.props.navigate('BoardDetail', { data: this.props.item })}>

        <View style={Theme.styles.newsItem}>
          <Text style={Theme.styles.textTopic}>{subject}</Text>
          <Text style={Theme.styles.miniText}>{new Date(date).toLocaleString('th-TH', { timeZone: 'Asia/Bangkok' })}</Text>
        </View>
      </TouchableOpacity>
    );
  }
}