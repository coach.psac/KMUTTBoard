import firebase from 'firebase';
import { Toast } from 'native-base';
import { Alert } from 'react-native'

import Strings from '../environments/Strings'
import Values from '../environments/Values'

firebase.initializeApp({
    apiKey: "AIzaSyDgMrdbDLxZurBQ_grT0CYrOeMDlXbuyY4",
    authDomain: "kmuttboard.firebaseapp.com",
    databaseURL: "https://kmuttboard.firebaseio.com",
    projectId: "kmuttboard",
    storageBucket: "kmuttboard.appspot.com",
    messagingSenderId: "214590803618"
})

function checkAuth(navigation) {
    firebase
        .auth()
        .onAuthStateChanged(function (user) {
            if (user) {
                navigation.navigate('Main')
            } else {
                navigation.navigate('Login')
            }
        });
}

function getUid() {
    return firebase
        .auth()
        .currentUser
        .uid
}

function register(navigation, email, password, value) {
    firebase
        .auth()
        .createUserWithEmailAndPassword(email, password)
        .then(() => {
            alert(Strings.english.registerComp)
            addUser(getUid(), value)
            navigation.navigate('Main')
        })
        .catch((err) => {
            Alert.alert('Oop!', err.message, [
                {
                    text: 'OK',
                    style: 'cancel'
                }
            ], { cancelable: false })
        })

}

function login(navigation, email, password) {
    firebase
        .auth()
        .signInWithEmailAndPassword(email, password)
        .then(() => {

            navigation.navigate('Main')
        })
        .catch((err) => {
            if (email || password)
                Alert.alert('Oops!', err.message, [
                    {
                        text: 'OK',
                        style: 'cancel'
                    }
                ], { cancelable: false })
            else
                Alert.alert('Oops!', Strings.english.loginErr, [
                    {
                        text: 'OK',
                        style: 'cancel'
                    }
                ], { cancelable: false })
        })

}

function logout(navigation) {
    Alert.alert('Logout', Strings.english.goLogout, [
        {
            text: 'No',
            style: 'cancel'
        }, {
            text: 'Yes',
            onPress: () => {
                serviceLogout(navigation)
            }
        }
    ], { cancelable: false })
}

function serviceLogout(navigation) {
    firebase
        .auth()
        .signOut()
        .then(function () {
            alert(Strings.english.logout)
            navigation.popToTop()
        })
        .catch(function (error) {
            alert(error)
        });
}

function getCurrentUser() {
    return findUserByUid(firebase.auth().currentUser.uid);
}

function findUserByUid(uid) {
    return firebase
        .database()
        .ref('users/' + uid)
        .once('value')
        .then(function (snapshot) {
            return snapshot.val();
        });
}

function compareUserByUid(uid1, uid2) {
    return uid1 == uid2
        ? true
        : false;
}

function isCurrentUserById(uid) {
    return compareUserByUid(firebase.auth().currentUser.uid, uid);
}

function post(ref, author, subject, message) {
    firebase
        .database()
        .ref(ref)
        .push({
            authorId: firebase
                .auth()
                .currentUser
                .uid,
            author: author,
            subject: subject,
            message: message,
            date: firebase.database.ServerValue.TIMESTAMP
        });
}

function comment(ref, commentStudentId, commentName, commentMessage) {
    firebase
        .database()
        .ref('comments/' + ref)
        .push({
            authorId: firebase
                .auth()
                .currentUser
                .uid,
            commentStudentId: commentStudentId,
            commentName: commentName,
            commentMessage: commentMessage,
            date: firebase.database.ServerValue.TIMESTAMP
        });
}

function remove(ref) {
    firebase
        .database()
        .ref(ref)
        .remove();
}

function addUser(userId, value) {
    firebase
        .database()
        .ref('users')
        .child(userId)
        .set({
            email: value.email,
            studentId: value.studentId,
            firstname: value.firstname,
            lastname: value.lastname,
            faculty: value.faculty,
            branch: value.branch,
            mobile: value.mobile
        })
}

function updateUser(navigation, data) {
    addUser(getUid(), data)
    alert('Saved')
    navigation.popToTop()
    navigation.push('Main')
}

function listenOrderByKey(ref, callback) {
    return firebase
        .database()
        .ref(ref)
        .orderByKey()
        .on('value', (snap) => {
            callback(snap)
        })
}

export default {
    firebase,
    checkAuth,
    register,
    login,
    getCurrentUser,
    logout,
    post,
    comment,
    remove,
    listenOrderByKey,
    getUid,
    updateUser,
    findUserByUid,
    compareUserByUid,
    isCurrentUserById
};