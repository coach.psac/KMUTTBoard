import React, {Component} from 'react'
import {TouchableOpacity, StyleSheet} from 'react-native';
import {
  Button,
  Container,
  Header,
  Content,
  Form,
  Item,
  Input,
  View,
  Text
} from 'native-base';

import IconMaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import FirebaseServices from '../services/FirebaseServices'
import Theme from '../environments/Theme'
import Values from '../environments/Values'

export default class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: ''
    }
  }

  componentWillMount() {
    FirebaseServices.checkAuth(this.props.navigation)
  }

  static navigationOptions = {
    header: null
  }

  onPressSignUp = () => {
    this
      .props
      .navigation
      .navigate('Register')
  }

  onPressLogin = () => {
    FirebaseServices.login(this.props.navigation, this.state.email, this.state.password)
  }

  render() {
    return (
      <View style={Theme.styles.containerLogin}>
        <Text style={Theme.styles.headerLogin}>KMUTT {'\n'}Board</Text>
        <Item rounded style={Theme.styles.inputLogin}>
          <IconMaterialCommunityIcons
            color={Values.colors.grey}
            size={Values.fontSize.icon}
            name="email"/>
          <Input
            placeholder="Email"
            value={this.state.email}
            onChangeText={(input) => this.setState({
            email: input.trim()
          })}/>
        </Item>
        <Item rounded style={Theme.styles.inputLogin}>
          <IconMaterialCommunityIcons
            color={Values.colors.grey}
            size={Values.fontSize.icon}
            name="lock"/>
          <Input
            placeholder="Password"
            secureTextEntry
            value={this.state.password}
            onChangeText={(input) => this.setState({password: input})}/>
        </Item>
        <Button
          style={Theme.styles.buttonLogin}
          light
          rounded
          onPress={this.onPressLogin}>
          <Text>Sign in</Text>
        </Button>

        <View style={{
          flexDirection: 'row'
        }}>
          <Text>Not a user?
          </Text>
          <TouchableOpacity onPress={this.onPressSignUp}>
            <Text style={Theme.styles.textRegister}>Register Here</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}