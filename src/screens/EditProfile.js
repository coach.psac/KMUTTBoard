import React, { Component } from 'react'
import { StyleSheet, View, ScrollView } from 'react-native';
import { Button, Text } from 'native-base'
import t from 'tcomb-form-native';

import FirebaseServices from '../services/FirebaseServices';

import Theme from './../environments/Theme';
import Strings from '../environments/Strings';
import Values from '../environments/Values';

const Form = t.form.Form;
const yearArr = () => {
    year = {}
    for (i = 0; i < 60; i++) {
        year['' + i] = 2561 - i + ""
    }
    return year
}
var Year = t.enums(yearArr());

const User = t.struct({
    email: t.String,
    studentId: t.Number,
    firstname: t.String,
    lastname: t.String,
    faculty: t.String,
    branch: t.String,
    mobile: t.String
});

const formStyles = {
    ...Form.stylesheet,
    formGroup: {
        normal: {
            marginBottom: 10
        }
    },
    controlLabel: {
        normal: Theme.styles.form,
        error: Theme.styles.error
    }
}

export default class EditProfile extends Component {
    constructor(props) {
        super(props)
        this.state = {
            value: {
                firstname: '',
                lastname: '',
                studentId: '',
                faculty: '',
                branch: '',
                mobile: ''
            }
        };
    };
    static navigationOptions = {
        title: 'Edit Profile',
        headerTitleStyle: Theme.styles.textHeader
    }

    componentWillMount() {
        this._load();
    }

    _load = () => {
        FirebaseServices
            .getCurrentUser()
            .then((data) => {
                const item = {
                    email: data.email,
                    firstname: data.firstname,
                    lastname: data.lastname,
                    studentId: data.studentId,
                    faculty: data.faculty,
                    branch: data.branch,
                    mobile: data.mobile
                }
                this.setState({ value: item })
            })
    }
    _onSave = () => {
        const value = this
            .refs
            ._form
            .getValue()
        console.log('value', value)
        console.log('saved')
        if (value) {
            FirebaseServices.updateUser(this.props.navigation, value);
            this._load()
        }
    }

    render() {
        let options = {
            fields: {
                email: {
                    label: 'Email:',
                    autoCapitalize: 'none',
                    error: Strings.english.emailErr,
                    editable: false
                },
                password: {
                    label: 'Password:',
                    secureTextEntry: true,
                    maxLength: 12,
                    error: Strings.english.passwordErr,
                    editable: false,
                    hidden: true
                },
                firstname: {
                    label: 'First name:',
                    error: Strings.english.fnameErr
                },
                lastname: {
                    label: 'Last name:'
                },
                studentId: {
                    label: 'Student ID',
                    maxLength: 11,
                    error: Strings.english.stdIdErr
                },
                faculty: {
                    label: 'Faculty:'
                },
                branch: {
                    label: 'Branch:'
                },
                mobile: {
                    label: 'Mobile:',
                    maxLength: 10
                }
            },
            stylesheet: formStyles
        }
        return (
            <View style={Theme.styles.container}>
                <ScrollView style={Theme.styles.w100}>
                    <View style={Theme.styles.p10}>
                        <Form ref='_form' type={User} options={options} value={this.state.value} />
                        <Button full warning onPress={this._onSave}>
                            <Text>Save</Text>
                        </Button>
                    </View>
                </ScrollView>
            </View>
        );
    }
}