import React, {Component} from 'react'
import {StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import {Container, Header, Tab, Tabs, TabHeading} from 'native-base';

import IconIonicons from 'react-native-vector-icons/Ionicons';
import IconMaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

import Strings from '../environments/Strings';
import Theme from './../environments/Theme';
import Values from './../environments/Values';

import News from './../tabs/News';
import Board from './../tabs/Board';
import Profile from './../tabs/Profile';

const RightButton = ({hasButton, title, navigation}) => {
    if (hasButton) {
        return (<IconIonicons
            style={Theme.styles.textHeader}
            name="md-add"
            onPress={() => {
            navigation.navigate(title + 'Insert');
        }}/>)
    } else if (title === Strings.english.profile) {
        return (<IconMaterialCommunityIcons
            style={Theme.styles.textHeader}
            name='account-edit'
            onPress={() => navigation.navigate('EditProfile')}/>)
    }
    return (null)
}

export default class Main extends Component {
    constructor(props) {
        super(props);
    }
    static navigationOptions = ({navigation}) => {
        let params = navigation.state.params || {}
        let {title, hasButton} = params
        return {
            headerLeft: <Text style={Theme.styles.textHeader}>{title}</Text>,
            headerRight: <RightButton hasButton={hasButton} title={title} navigation={navigation}/>
        }
    }

    componentWillMount() {
        this
            .props
            .navigation
            .setParams({title: Strings.english.news, hasButton: true});
    }

    _onTabChange = (tab) => {
        let {navigation} = this.props
        let title = ''
        let hasButton = true

        switch (tab.i) {
            case 0:
                title = Strings.english.news
                break;
            case 1:
                title = Strings.english.board
                break;
            case 2:
                title = Strings.english.profile
                hasButton = false
                break;
        }
        navigation.setParams({title: title, hasButton: hasButton})
    }

    render() {
        return (
            <View style={Theme.styles.container}>
                <Tabs
                    tabBarPosition="bottom"
                    tabBarUnderlineStyle={{
                    backgroundColor: Values.colors.orange
                }}
                    onChangeTab={(tab) => this._onTabChange(tab)}>
                    <Tab
                        heading={(
                        <TabHeading><IconMaterialCommunityIcons size={Values.fontSize.icon} name="newspaper"/></TabHeading>
                    )}>
                        <News navigate={this.props.navigation.navigate}/>
                    </Tab>
                    <Tab
                        heading={(
                        <TabHeading><IconMaterialCommunityIcons size={Values.fontSize.icon} name="bulletin-board"/></TabHeading>
                    )}>
                        <Board navigate={this.props.navigation.navigate}/>
                    </Tab>
                    <Tab
                        heading={(
                        <TabHeading><IconIonicons size={Values.fontSize.icon} name="md-person"/></TabHeading>
                    )}>
                        <Profile navigation={this.props.navigation}/>
                    </Tab>
                </Tabs>
            </View>
        );
    }
}