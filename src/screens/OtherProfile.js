import React, { Component } from 'react'
import { StyleSheet, Text, View, TouchableOpacity, ScrollView, Button } from 'react-native';

import Theme from './../environments/Theme';
import Values from './../environments/Values';

import FirebaseServices from './../services/FirebaseServices';

export default class Profile extends Component {
    constructor(props) {
        super(props)
        this.state = { data: {} }

    }
    static navigationOptions = {
        title: 'Profile'
    }
    componentWillMount() {
        this._load()
    }

    _load = () => {
        // let { userId } = this.props.navigation.getParam('userId')
        // FirebaseServices.findUserByUid(userId).then((data) => { this.setState({ data: data }) })
    }

    render() {
        let { data } = this.state

        return (
            <View style={Theme.styles.container}>
                {/* <View style={[Theme.styles.w100, Theme.styles.flexRow]}>
                    <ScrollView>
                        <Item label="First name:" value={data.firstname} />
                        <Item label="Last name:" value={data.lastname} />
                        <Item label="Student ID:" value={data.studentId} />
                        <Item label="Faculty:" value={data.faculty} />
                        <Item label="Branch:" value={data.branch} />
                        <Item label="Started Year:" value={data.year} />
                        <Item label="Mobile No:" value={data.mobile} />
                    </ScrollView>
                </View> */}
            </View>
        );
    }
}
class Item extends Component {
    render() {
        return (
            <View style={[Theme.styles.newsItem, { flex: 1, flexDirection: 'row' }]}>
                <View style={Theme.styles.flex2label}>
                    <Text style={Theme.styles.textTopic}>{this.props.label}</Text>
                </View>
                <View style={{ flex: 3 }}>
                    <Text style={Theme.styles.textDetail}>
                        {this.props.value}
                    </Text>
                </View>
            </View>
        );
    }
}