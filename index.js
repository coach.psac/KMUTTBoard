import { AppRegistry } from 'react-native';
import { YellowBox } from 'react-native';
import App from './App';

YellowBox.ignoreWarnings(['Setting a timer']);
YellowBox.ignoreWarnings(['Warning: isMounted(...) is deprecated', 'Module RCTImageLoader']);

AppRegistry.registerComponent('KMUTTBoard', () => App);
